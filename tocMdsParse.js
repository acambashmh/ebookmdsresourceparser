var request = require('request');
var q = require('q');
var _ = require('lodash');
var fs = require('fs');
var cheerio = require('cheerio');

function parseMDSAttributeString(mdsAttrString) {
	var fullMdsArray = mdsAttrString.split(',');
	var parsedMDS = {};
	fullMdsArray.forEach(function(mdsAttrStringItem) {
		var temp = mdsAttrStringItem.split('-');
		parsedMDS[temp[0].trim()] = temp[1];
	});
	return parsedMDS;
}

function getAllMDSAttributes(path) {
	return q.nfcall(fs.readFile, path, "utf-8").then(function (data) {
		var $ = cheerio.load(data);
		var htmlList = $('li[data-mds]');
		var result = [];
		for (htmlItemKey in htmlList) {
			var htmlItem = htmlList[htmlItemKey];
			if (htmlItem.attribs) {
				var mdsValue = htmlItem.attribs['data-mds'];
				if (mdsValue) {
					result.push(htmlItem.attribs['data-mds']);
				}
			}
		}
		result = _.uniq(result);
		var temp = [];
		result.forEach(function (mdsString) {
			temp.push(parseMDSAttributeString(mdsString));
		});
		return temp;
	});
}

function compareResourceItemWithMdsAttr(resourceItem, mdsAttributeItem) {
	var compareItem = {};
	compareItem.L0 = resourceItem.grade;
	if (resourceItem.collection) {
		compareItem.L2 = resourceItem.collection.toString();
	} else if (resourceItem.unit) {
		compareItem.L2 = resourceItem.unit.toString();
	} else if (resourceItem.module) {
		compareItem.L2 = resourceItem.module.toString();
	}
	if (resourceItem.lesson) {
		compareItem.L5 = resourceItem.lesson.toString();
	}
	
	for (var key in mdsAttributeItem) {
		if (mdsAttributeItem.hasOwnProperty(key)) {
			mdsAttributeItem[key] = parseMdsAttrValue(mdsAttributeItem[key]);
		}
	}

	if (JSON.stringify(compareItem) === JSON.stringify(mdsAttributeItem)) {
		return true;
	} else {
		return false;
	}
}

function parseMdsAttrValue(mdsAttrValue) {
	if (mdsAttrValue[0] === '0') {
		return parseMdsAttrValue(mdsAttrValue.substring(1));
	} else {
		return mdsAttrValue;
	}
}

function isResourceForEbook(resourceItem, mdsResourceAttrsList) {
	for (var i = 0; i < mdsResourceAttrsList.length; i++) {
		var element = mdsResourceAttrsList[i];
		var res = compareResourceItemWithMdsAttr(resourceItem, element);
		if (res) {
			return true;
		}
	}
	return false;
}

function getListOfEbookResources(resourceList, mdsResourceAttrsList) {
	var result = [];
	
	resourceList.forEach(function (resourceItem) {
		if (isResourceForEbook(resourceItem, mdsResourceAttrsList)) {
			result.push(resourceItem);
		}
	});
	return result;
}

function getListOfEbookResourcesFromToc(resourceList, path) {
	return getAllMDSAttributes(path).then(function (mdsResourceAttrsList) {
		return getListOfEbookResources(resourceList, mdsResourceAttrsList);
	});
}

function getGradeFromMdsAttrs(mdsResourceAttrsList) {
	var result;

	if (mdsResourceAttrsList.length && mdsResourceAttrsList.length > 0) {
		if (mdsResourceAttrsList[0].L0) {
			result = mdsResourceAttrsList[0].L0;
		} else {
			throw new Error('wrong mds attrs passed, cant get grade');
		}
	} else if (mdsResourceAttrsList.L0) {
		result = mdsResourceAttrsList.L0;
	} else {
		throw new Error('wrong mds attrs passed, cant get grade');
	}
	result = parseMdsAttrValue(result);
	return result;
}

module.exports.isResourceForEbook = isResourceForEbook;
module.exports.getAllMDSAttributes = getAllMDSAttributes;
module.exports.compareResourceItemWithMdsAttr = compareResourceItemWithMdsAttr;
module.exports.getListOfEbookResources = getListOfEbookResources;
module.exports.getListOfEbookResourcesFromToc = getListOfEbookResourcesFromToc;
module.exports.parseMdsAttrValue = parseMdsAttrValue;
module.exports.getGradeFromMdsAttrs = getGradeFromMdsAttrs;
module.exports.parseMDSAttributeString = parseMDSAttributeString;