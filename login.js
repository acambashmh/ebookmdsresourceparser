var request = require('request');
var credentials = require('./credentialStore.json');
var q = require('q');

module.exports = function (username, password, platform, program) {
	var defer = q.defer();
	
	var uriTemplate;

	if (!platform || (platform !== 'hmof' && platform !== 'tc') || platform == 'hmof') {
		platform = 'hmof';
		uriTemplate = 'https://my-review-cert.hrw.com/api/identity/v1/token;contextId='; //cert review data
	}
	
	if(platform == 'tc') {
		// var suffix = program.match('NA Journeys')?(',o=88200347,dc=88200346,st=RI'):(',o=88200010,dc=88200009,st=CA');
		// var suffix = ',o=88200010,dc=88200009,st=CA';
		// var suffix = ',o=88200347,dc=88200346,st=RI';
            username = 'uid=' + username;// + suffix; //cert review data
			//  ,o=88200347,dc=88200346,st=RI
			uriTemplate = 'https://www-review-cert-tc1.thinkcentral.com/api/identity/v1/token;contextId=';
        }

	var uri = uriTemplate + platform;

	request.post(
		{ url: uri, 
			form: { 
					username: username,
					password: password,
					grant_type: 'password' 
				} 
			}, function (err, response, body) {
		if (err) {
			defer.reject(err);
		} else {
			try {
				body = JSON.parse(body);
				defer.resolve({token:body, "platform":platform});
			} catch (error) {
				defer.reject(error);
			}
			
		}
	})


	return defer.promise;
}