var fs = require('fs');
var chai = require('chai');
var chaiAsPromised = require("chai-as-promised");
var tocMdsParse = require('../tocMdsParse');
var mdsCalls = require('../mdsCalls');
var login = require('../login');


chai.use(chaiAsPromised);
chai.should();


var collectionLevelResources = require('./collectionLevelResources.json');
var mdsAttrs = require('./mdsAttrs.json');


describe('getting and converting MDS call result', function () {
	
	it('should convert mds resource call item into consumable object', function () {
		var item1= {
						"id" : "9780544581012-0023",
						"HMH_ID" : "ESC_NA17_SMG_G07U01L00_001",
						"resource_type" : "",
						"display_title" : "Browse Magazine: Unit 1: Bold Actions, 1",
						"display_color" : 0,
						"keywords" : {
							"keyword" : ["Browse Magazine", "Unit 1", "Bold Actions", "Unit 1 Browse Magazine", "Magazine", "articles", "independent reading", "comprehension", "academic language", "Brave But Scared?", "Careers for Bold People", "Be Bold", "Stand Out in the Crowd", "Bold People Who Risked Everything", "Bold Girls Can Change the World", "Braver Than Superman", "When Risk Doesn't Pay Off . . .", "Everyone is Bold in Different Ways"]
						},
						"tool_type" : 6,
						"language" : "en-US",
						"discipline" : {
							"code" : "10",
							"label" : "Other"
						},
						"setOfISBNs" : {
							"ISBN" : [{
									"type" : "primary",
									"value" : "9780544581012"
								}
							]
						},
						"strand" : 0,
						"reteach" : false,
						"diff_inst" : false,
						"meaningful_description" : "High-interest articles related to the Unit 1 topic, Bold Actions, provide independent reading opportunities for students to improve their comprehension and academic language skills. The articles for this unit are: <ul> <li>Brave But Scared?<\/li> <li>Careers for Bold People<\/li> <li>Be Bold: Stand Out in the Crowd<\/li> <li>Bold People Who Risked Everything<\/li> <li>Bold Girls Can Change the World<\/li> <li>Braver Than Superman<\/li> <li>When Risk Doesn't Pay Off . . .<\/li> <li>Everyone is Bold in Different Ways<\/li> <\/ul>",
						"additional_text" : "",
						"viewable" : true,
						"assignable" : true,
						"schedulable" : true,
						"searchable" : true,
						"teacher_managed" : false,
						"se_facing" : true,
						"enrich" : false,
						"component" : "Browse Magazine",
						"component_type" : "Key Student Resource",
						"media_type" : "HTML",
						"categorization" : "Core Components",
						"hierarchy" : 9,
						"title" : "",
						"genres" : "",
						"freeplay" : true,
						"active" : false,
						"persistent" : false,
						"doneOwner" : "",
						"secondaryURLs" : {
							"secondaryURL" : [{
									"title" : "",
									"type" : "",
									"URL" : ""
								}, {
									"title" : "",
									"type" : "",
									"URL" : ""
								}
							]
						},
						"lab" : {
							"type" : "",
							"class_time" : {
								"text" : "",
								"scale" : 0
							},
							"prep_time" : {
								"text" : "",
								"scale" : 0
							},
							"difficulty" : {
								"text" : "",
								"scale" : 0
							},
							"materials" : {
								"text" : "",
								"scale" : 0
							}
						},
						"theme" : "",
						"text_complexity" : "",
						"IWB_Compatible" : false,
						"ext" : "",
						"resources_panel_te" : false,
						"resources_panel_se" : true,
						"non_grade_level" : "",
						"non_grade_title" : "",
						"download_url" : "",
						"parent_resource_id" : "23",
						"sort_id" : "",
						"author" : {
							"author" : ""
						},
						"standards" : {
							"standard" : [""]
						},
						"uri" : {
							"relative_url" : "\/content\/hmof\/language_arts\/escalate_english\/na\/gr7\/magazine_9780544581371_\/index.html?page=1",
							"platform_url" : "http:\/\/www.myhrw.com"
						},
						"icon_url" : {
							"relative_icon_url" : "",
							"platform_url" : "http:\/\/www.myhrw.com"
						},
						"component_id" : 1935,
						"resource_type_id" : 1,
						"media_type_id" : 2,
						"component_type_id" : 7,
						"categorization_id" : 6,
						"reader" : null,
						"levels" : {
							"level" : [{
									"hierarchy" : 12,
									"type" : "grade",
									"level_number" : 0,
									"grades" : {
										"grade" : ["7"]
									},
									"title" : ""
								}, {
									"hierarchy" : 1,
									"type" : "unit",
									"level_number" : 2,
									"title" : "Bold Actions",
									"instructional_segment" : {
										"hierarchy" : 1,
										"strand_type" : [{
												"hierarchy" : 8,
												"title" : "Unit-Level Resources",
												"strand_type_id" : null
											}
										],
										"title" : "Student Resources",
										"instructional_segment_id" : 458
									}	
								}
							]
						},
						"programs" : {
							"program" : ["Escalate English 2017"]
						}
					};
		
		var res1 = mdsCalls.parseMDSResourceListItem(item1);
		//console.log(res1);
		res1.grade.should.equal('7');
		res1.unit.should.equal('1');
	});
	
})


describe('Compare MDS toc.xhtml attributes and MDS resources', function () {

	it('should successfully parse full mds attribute string', function () {
		var mdsAttrString1 = 'L0-006,L2-001,L5-002';
		var mdsAttrString2 = 'L0-006';
		var mdsAttrString3 = 'L0-006,L1-001,L2-001,L5-001';
		var mdsAttrString4 = 'L0-006,L1-001,L2-001,L5-001,L6-007';
		
		var res1 = tocMdsParse.parseMDSAttributeString(mdsAttrString1);
		res1.L0.should.equal('006');
		res1.L2.should.equal('001');
		res1.L5.should.equal('002');
		
		var res2 = tocMdsParse.parseMDSAttributeString(mdsAttrString2);
		res2.L0.should.equal('006');
		
		var res3 = tocMdsParse.parseMDSAttributeString(mdsAttrString3);
		res3.L0.should.equal('006');
		res3.L1.should.equal('001');
		res3.L2.should.equal('001');
		res3.L5.should.equal('001');
		
		var res4 = tocMdsParse.parseMDSAttributeString(mdsAttrString4);
		res4.L0.should.equal('006');
		res4.L1.should.equal('001');
		res4.L2.should.equal('001');
		res4.L5.should.equal('001');
		res4.L6.should.equal('007');
		
	});
	

	it('should parse value 005 to 5 and 010 to 10', function () {

		var res1 = tocMdsParse.parseMdsAttrValue('005');
		var res2 = tocMdsParse.parseMdsAttrValue('010');

		res1.should.equal('5');
		res2.should.equal('10');
    });
    
	it('should successfully validate Escalate english resurce item based on one MDS attr value', function () {
        var resource = {
			"grade": "6",
			"collection" : 1,
		};
		var mdsAttr = {
			L0: '006',
			L2: '001',
		}
        var res = tocMdsParse.compareResourceItemWithMdsAttr(resource, mdsAttr);
		res.should.equal(true);
    });

    it('should successfully validate resurce item based on one MDS attr value', function () {
        var resource = {
			"HMH_ID": "LTCA17_G06C01L05_CA_002",
			"display_title": "Close Read Application: Fears and Phobias (lines 147-162)",
			"uri": {
				"relative_url": "/content/hmof/language_arts/hmhcollections/resources/gr6/close_read_applications/lit15_g06_c01_l05_fears_cra.pdf",
				"platform_url": "http://my.hrw.com"
			},
			"categorization": "Teaching Aids",
			"media_type": "pdf",
			"component_type": "ancillary",
			"component": "Close Read Application",
			"id": "9780544481329-00476",
			"grade": "6",
			"collection": 1,
			"lesson": 5
		};
		var mdsAttr = {
			L0: '006',
			L2: '001',
			L5: '005'
		}
        var res = tocMdsParse.compareResourceItemWithMdsAttr(resource, mdsAttr);
		res.should.equal(true);
    });
	
	
	
	it('should fail validate resurce item', function () {
        var resource1 = {
			"id": "9780544481329-00476",
			"grade": "6",
			"collection": 1,
			"lesson": 5
		};
		var resource2 = {
			"id": "9780544481329-00476",
		};
		var resource3 = {
			"id": "9780544481329-00476",
			"collection": 1,
			"lesson": 5
		};
		var resource4 = {
			"id": "9780544481329-00476",
			"lesson": 5
		};
		var resource5 = {
			"id": "9780544481329-00476",
			"collection": 1,
		};
		var resource6 = {
			"id": "9780544481329-00476",
			"grade": "6",
			"collection": 2,
			"lesson": 4
		};

		var mdsAttr = {
			L0: '006',
			L2: '001',
			L5: '004'
		}
        var res1 = tocMdsParse.compareResourceItemWithMdsAttr(resource1, mdsAttr);
		var res2 = tocMdsParse.compareResourceItemWithMdsAttr(resource2, mdsAttr);
		var res3 = tocMdsParse.compareResourceItemWithMdsAttr(resource3, mdsAttr);
		var res4 = tocMdsParse.compareResourceItemWithMdsAttr(resource4, mdsAttr);
		var res5 = tocMdsParse.compareResourceItemWithMdsAttr(resource5, mdsAttr);
		var res6 = tocMdsParse.compareResourceItemWithMdsAttr(resource6, mdsAttr);
		res1.should.equal(false);
		res2.should.equal(false);
		res3.should.equal(false);
		res4.should.equal(false);
		res5.should.equal(false);
		res6.should.equal(false);
    });

	it('should successfuly validate resource item based on list of MDS attrs', function () {
		var resource = {
			"HMH_ID": "LTCA17_G06C01L05_CA_002",
			"display_title": "Close Read Application: Fears and Phobias (lines 147-162)",
			"uri": {
				"relative_url": "/content/hmof/language_arts/hmhcollections/resources/gr6/close_read_applications/lit15_g06_c01_l05_fears_cra.pdf",
				"platform_url": "http://my.hrw.com"
			},
			"categorization": "Teaching Aids",
			"media_type": "pdf",
			"component_type": "ancillary",
			"component": "Close Read Application",
			"id": "9780544481329-00476",
			"grade": "6",
			"collection": 1,
			"lesson": 5
		};
		var res = tocMdsParse.isResourceForEbook(resource, mdsAttrs);
		res.should.equal(true);
	});

	it('should fail to validate resource item based on list of MDS attrs', function () {
		var resource = {
			"HMH_ID": "LTCA17_G06C01L05_CA_002",
			"display_title": "Close Read Application: Fears and Phobias (lines 147-162)",
			"uri": {
				"relative_url": "/content/hmof/language_arts/hmhcollections/resources/gr6/close_read_applications/lit15_g06_c01_l05_fears_cra.pdf",
				"platform_url": "http://my.hrw.com"
			},
			"categorization": "Teaching Aids",
			"media_type": "pdf",
			"component_type": "ancillary",
			"component": "Close Read Application",
			"id": "9780544481329-00476",
			"grade": "6",
			"collection": 13,
			"lesson": 5
		};
		var res = tocMdsParse.isResourceForEbook(resource, mdsAttrs);
		res.should.equal(false);
	});

	it('should validate resource list based on list of MDS attrs', function () {
		var res = tocMdsParse.getListOfEbookResources(collectionLevelResources, mdsAttrs);
		res.length.should.equal(17);
	});

	it('should get the grade value 6 from list of MDS attrs', function () {
		var mdsAttr = {
			L0: '006',
			L2: '001',
			L5: '004'
		}
		tocMdsParse.getGradeFromMdsAttrs(mdsAttr).should.equal('6');
		tocMdsParse.getGradeFromMdsAttrs([mdsAttr]).should.equal('6');
	});

});

describe('should test api and file calls', function () {
	it('should login to int', function (done) {
		login('Ent9477@6y', 'Ent9477@6y').then(function (tokenData) {
			if (tokenData.access_token) {
				return true;
			} else {
				return false;
			}
		}).should.eventually.equal(true).notify(done);
	});

})

