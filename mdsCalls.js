var request = require('request');
var q = require('q');
var _ = require('lodash');
var fs = require('fs');
var cheerio = require('cheerio');
var url = require('url');
function getGradeResourcesFromMds(program, grade, sifToken, platform, role, mdsAttrs){
	var urlPath = parsePathForMdsGradeCall(program, grade, platform, role, mdsAttrs);

	return multiHitWithParse(urlPath, sifToken);
}


function parsePathForMdsGradeCall(program, grade, platform, role, mdsAttrs){
	console.log(grade);
	if(grade === 'KG'){
		
	}else if(grade.length === 2){
		grade = '0'+grade;
	}else if(grade.length === 1){
		grade = '0'+grade;
	}else if (grade.length === 3){
		
	}else{
		throw new Error('wrong grade data, cant parse correct mds call');
	}
	if(!program){
		throw new Error('wrong program, cant parse correct mds call');
	}
	var mdsEndpoint;
	if(!role || !(role == 'te' || role == 'se')){
		role = 'te';
	}
	if(platform == 'hmof'){
		mdsEndpoint = 'https://my-review-cert.hrw.com/';
	}
	else if(platform == 'tc'){
		mdsEndpoint = 'https://www-review-cert-tc1.thinkcentral.com/';
	}
	// var mdsUrlCall = mdsEndpoint + 'api/search/metadata/?q=(program:'+encodeURIComponent('"'+ program+'"')+'+grade:'+ grade + '+resources_panel_'+ role +':true)&collection=Resources&options=search-ebook-json&format=json';
	var mdsUrlCall = generateMultiPathHits(mdsAttrs, mdsEndpoint, program, platform, grade, role);
	// var mdsUrlCall = 'http://my-test.hrw.com/api/search/metadata/?q=(program:'+encodeURIComponent('"'+ program+'"')+'+grade:'+ grade + '+resources_panel_se:true)&collection=Resources&options=search-ebook-json&format=json';
	console.log(mdsUrlCall);
	// return [mdsUrlCall];
	return mdsUrlCall;
}

function multiHit(paths, sifToken) {
	var getAllPromiseArray = [];
	paths.forEach(function (path) {
		var options = {
			url: path,
			method:'GET',
			headers: {
				'Authorization': sifToken
			}
		};
		request(options, function(error, response, body){
			if(error){
				console.log(error);
				defer.reject(error);
			}else{
				defer.resolve(JSON.parse(body));
			}
		});
		var defer = q.defer();
		getAllPromiseArray.push(defer.promise);
	});

	return q.all(getAllPromiseArray)
}

function generateMultiPathHits(mdsAttrs, mdsEndpoint, program, platform, grade, role){
	if(platform != 'tc'){
		return [mdsEndpoint + 'api/search/metadata/?q=(program:'+encodeURIComponent('"'+ program+'"')+'+grade:'+ grade + '+resources_panel_'+ role +':true)&collection=Resources&options=search-ebook-json&format=json'];
	}
	else{
		var outputPaths = [];
		for(var i=0;i<mdsAttrs.length;i++){
			var endpoint = mdsEndpoint + 'api/search/metadata/?q=(program:'+encodeURIComponent('"'+ program+'"')+'+grade:'+ grade;
			if(mdsAttrs[i].L2){
				endpoint += '+unit:' + mdsAttrs[i].L2; 
			}
			if(mdsAttrs[i].L5){
				endpoint += '+lesson:' + mdsAttrs[i].L5; 
			}
			endpoint+= '+resources_panel_'+ role +':true)&collection=Resources&options=search-ebook-json&format=json';
			outputPaths.push(endpoint);
		}
		return outputPaths;
	}
}


function parseMDSResult(response){
	var temp = []
	 response.forEach(function(responseItem){
		 console.log(responseItem);
		temp.push(parseMDSResourceList(responseItem.results));
	});
	var arrayOfAllLessonResources = _.flatten(temp);
	return arrayOfAllLessonResources;
}

function parseMDSResourceList(results){
	var result = [];
	results.forEach(function(item){
		if(item.content.resource.length > 1){
			console.log('resource issue');
			console.log(item.content.resource)
		}
		var resource = item.content.resource[0];
		result.push(parseMDSResourceListItem(resource));
		
	});
	return result;
}

function parseMDSResourceListItem(resource){
		var result = {
			HMH_ID : resource.HMH_ID,
			display_title : resource.display_title,
			uri : resource.uri,
			categorization : resource.categorization,
			media_type : resource.media_type,
			component_type : resource.component_type,
			component : resource.component,
			id : resource.id,
			resources_panel_se:resource.resources_panel_se,
			resource_type:resource.resource_type,
			tool_type:resource.tool_type,
			viewable:resource.viewable,
			assignable:resource.assignable,
			se_facing:resource.se_facing,
			download_url:resource.download_url,
			categorization_id:resource.categorization_id,
			media_type_id:resource.media_type_id,
			component_type_id:resource.component_type_id,
			resource_type_id:resource.resource_type_id
		}
		resource.levels.level.forEach(function(element) {
			var levelName = element.type;
			var levelValue =  element.hierarchy || 'someLvl';
			if(element.grades && element.grades.grade &&  element.grades.grade.length > 0){
				levelValue = element.grades.grade[0];
			}
			if(levelValue){
				levelValue = levelValue.toString();
			}
			result[levelName] = levelValue || null; 
		});
		
		return result;
}


function multiHitWithParse(paths, sifToken) {
	return multiHit(paths, sifToken).then(function (response) {
		return parseMDSResult(response);
	});
}



module.exports.parseMDSResourceList = parseMDSResourceList;
module.exports.parseMDSResult = parseMDSResult;
module.exports.multiHit = multiHit;
module.exports.multiHitWithParse = multiHitWithParse;
module.exports.getGradeResourcesFromMds = getGradeResourcesFromMds;
module.exports.parseMDSResourceListItem = parseMDSResourceListItem;