var q = require('q');
var getEbookResources = require('./getEbookResources');
var fs = require('fs');
var path = require('path');

module.export = getEbookResources;

var argv = {};

var credentials = require('./credentialStore.json');

// argv.username = 'collte';
// argv.password = 'Password@1'
// argv.program = 'HMH Collections 2017';
// argv.tocpath = 'C:/SVN/collgr6te/OPS/toc.xhtml';
// argv.configpath = 'C:/SVN/collgr6te/OPS/config.txt';
// argv.platform = 'hmof';

// argv.username = 'COLLTE';
// argv.password = 'Password@1'
// argv.program = 'HMH Collections 2017';
// argv.tocpath = 'E:/ELAContent/Collections/collgr8se/OPS/toc.xhtml';
// argv.configpath = 'E:/ELAContent/Collections/collgr8se/OPS/config.txt';
// argv.platform = 'hmof';
// argv.role = 'se';

// argv.username = 'ent10577r,o=88200010,dc=88200009,st=CA';
// argv.password = 'Ent10577@2'
// argv.platform = 'tc';
// argv.tocpath = 'E:/ELAContentNA/Journeys/jrngr3se/vol1/OPS/toc.xhtml';
// argv.configpath = 'E:/ELAContentNA/Journeys/jrngr3se/vol1/OPS/config.txt';
// argv.role = 'se';

argv.username = 'ent10577r,o=88200010,dc=88200009,st=CA';
argv.password = 'Ent10577@2'
argv.platform = 'tc';
argv.tocpath = 'E:/ELAContent/Journeys/jrngr3se/vol1/OPS/toc.xhtml';
argv.configpath = 'E:/ELAContent/Journeys/jrngr3se/vol1/OPS/config.txt';
argv.role = 'se';

// argv.username = 'ent11278@3';
// argv.password = 'Ent11278@3'
// argv.platform = 'tc';
// argv.tocpath = 'E:/ELAContent/Journeys/jrngr4se/Build/OPS/toc.xhtml';
// argv.configpath = 'E:/ELAContent/Journeys/jrngr4se/Build/OPS/config.txt';
// argv.role = 'se';

// argv.username = 'NACOLLTE1';
// argv.password = 'Password@1'
// argv.platform = 'hmof';
// argv.program = 'HMH ESCALATE 2017';
// argv.tocpath = 'E:/ELAContent/EscalateEnglish/escgr6se/OPS/toc.xhtml';
// argv.configpath = 'E:/ELAContent/EscalateEnglish/escgr6se/OPS/config.txt';
// argv.platform = 'tc';

// getEbookResources.test('ggg');

if (argv.username && argv.password && argv.tocpath && argv.configpath) {
	console.log('username - ' + argv.username);
	console.log('password - ' + argv.password);
	console.log('configpath - ' + argv.configpath);
	console.log('tocpath - ' + argv.tocpath);
	console.log('platform - ' + argv.platform);

	getEbookResources(argv.username, argv.password, argv.configpath, argv.tocpath, argv.platform, argv.role)
		.then(function (data) {
			var filePath = path.resolve(__dirname + '/result.json')
			console.log('Dowloaded list with ' + data.length + ' resources and saved them in ' + filePath);

			fs.writeFileSync(filePath, JSON.stringify(data, null,2));
		}).then(function (err) {
			console.log(err);
		})
}



