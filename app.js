var q = require('q');
var getEbookResources = require('./getEbookResources');
var fs = require('fs');
var path = require('path');
var argv = require('yargs').argv;

module.exports = getEbookResources;
if(argv.username && argv.password && argv.configpath && argv.tocpath){
	console.log('username - ' + argv.username);
	console.log('password - ' + argv.password);
	console.log('configpath - ' + argv.configpath);
	console.log('tocpath - ' + argv.tocpath);
	
	getEbookResources(argv.username , argv.password , argv.configpath ,argv.tocpath, argv.platform).then(function (data) {
		var filePath = path.resolve(__dirname+'/result.json')
		console.log('Dowloaded list with '+data.length + ' resources and saved them in ' + filePath);
		
		fs.writeFileSync(filePath, JSON.stringify(data));
		return;
	}).then(function (err) {
		console.log(err);
	})
}



