var tocMdsParse = require('./tocMdsParse');
var mdsCalls = require('./mdsCalls');
var login = require('./login');
var q = require('q');
var fs = require('fs');

module.exports = function (username, password, configPath, tocPath, platform, role) {
	var sifToken = null;
	var mdsAttrs = null;
	var grade = null;
	var program = "";
	var settings = null;

	return q.fcall(function () {
		var defer = q.defer();
		
		fs.readFile(configPath, 'utf-8', function (err, data) {
			if (err) {
				return defer.reject(err);
			}
			try {
				var parsedConfig = JSON.parse(data);
				if (parsedConfig.Settings && parsedConfig.Settings.MDSProgramName) {
					settings = parsedConfig.Settings;
					defer.resolve(parsedConfig.Settings.MDSProgramName);
				} else {
					return defer.reject('The config file doesnt have program name');
				}
			} catch (err) {
				return defer.reject('Error with parsing config file');
			}

		});

		return defer.promise;
	})
		.then(function (programResult) {
			program = programResult.toString();
			return tocMdsParse.getAllMDSAttributes(tocPath);
		})
		.then(function (mdsAttrsResult) {
			mdsAttrs = mdsAttrsResult;
			grade = tocMdsParse.getGradeFromMdsAttrs(mdsAttrsResult);
			return;
		}).then(function () {
			return login(username, password, platform, program);
		}).then(function (tokenData) {
			console.log('token', tokenData.platform);
			sifToken = tokenData.token.access_token;
			return mdsCalls.getGradeResourcesFromMds(program, grade, sifToken, platform, role, mdsAttrs);
		}).then(function (allResources) {
			return tocMdsParse.getListOfEbookResources(allResources, mdsAttrs);
		}).catch(function (err) {
			console.log(err);
			throw 'Error while getting mds ebook resources, err: ' + JSON.stringify(err);
		});
};