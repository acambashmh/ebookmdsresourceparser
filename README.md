#mds Ebook parser
This app provides all the resources that can be obtained from the MDS for an eBook on a grade level 

##setup
npm install

to use it from the console 
node app --username someUsername --password somePassword --configpath someConfigPath --tocpath someTocPath --platform (tc/hmof hmof is default)

example:
	node app --username Ent9477@6y --password Ent9477@6y --configpath ./test/program-collections2017-grade-6-version-se-config.txt --tocpath ./test/program-collections2017-grade-6-version-se.xhtml

or you can require it and use the promise API method

	var getEbookResources = require('name of module');
	getEbookResources(username,password,configpath,pathToTocXhtml,platform).then(function(ebookResources){
		//...
	})